﻿namespace winformDetectObject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_Brows = new System.Windows.Forms.Button();
            this.btn_Start = new System.Windows.Forms.Button();
            this.btn_Stop = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.trackBarThreshold = new System.Windows.Forms.TrackBar();
            this.label1Threshold = new System.Windows.Forms.Label();
            this.lbSmooth = new System.Windows.Forms.Label();
            this.trackSmooth = new System.Windows.Forms.TrackBar();
            this.trackDilate = new System.Windows.Forms.TrackBar();
            this.lbDilate = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.picROI = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackSmooth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackDilate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picROI)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(6, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1280, 720);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btn_Brows
            // 
            this.btn_Brows.Location = new System.Drawing.Point(12, 12);
            this.btn_Brows.Name = "btn_Brows";
            this.btn_Brows.Size = new System.Drawing.Size(75, 23);
            this.btn_Brows.TabIndex = 1;
            this.btn_Brows.Text = "Brows";
            this.btn_Brows.UseVisualStyleBackColor = true;
            this.btn_Brows.Click += new System.EventHandler(this.btn_Brows_Click);
            // 
            // btn_Start
            // 
            this.btn_Start.Location = new System.Drawing.Point(93, 12);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(75, 23);
            this.btn_Start.TabIndex = 2;
            this.btn_Start.Text = "Start";
            this.btn_Start.UseVisualStyleBackColor = true;
            this.btn_Start.Click += new System.EventHandler(this.btn_Start_Click);
            // 
            // btn_Stop
            // 
            this.btn_Stop.Location = new System.Drawing.Point(255, 12);
            this.btn_Stop.Name = "btn_Stop";
            this.btn_Stop.Size = new System.Drawing.Size(75, 23);
            this.btn_Stop.TabIndex = 3;
            this.btn_Stop.Text = "Stop";
            this.btn_Stop.UseVisualStyleBackColor = true;
            this.btn_Stop.Click += new System.EventHandler(this.btn_Stop_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(174, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Pause";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btn_Pause_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 41);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1309, 767);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1301, 741);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pictureBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1301, 741);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(10, 10);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1280, 720);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // trackBarThreshold
            // 
            this.trackBarThreshold.Location = new System.Drawing.Point(466, 12);
            this.trackBarThreshold.Maximum = 250;
            this.trackBarThreshold.Minimum = 10;
            this.trackBarThreshold.Name = "trackBarThreshold";
            this.trackBarThreshold.Size = new System.Drawing.Size(195, 45);
            this.trackBarThreshold.SmallChange = 10;
            this.trackBarThreshold.TabIndex = 10;
            this.trackBarThreshold.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarThreshold.Value = 170;
            this.trackBarThreshold.Scroll += new System.EventHandler(this.trackBarThreshold_Scroll);
            // 
            // label1Threshold
            // 
            this.label1Threshold.AutoSize = true;
            this.label1Threshold.Location = new System.Drawing.Point(382, 22);
            this.label1Threshold.Name = "label1Threshold";
            this.label1Threshold.Size = new System.Drawing.Size(78, 13);
            this.label1Threshold.TabIndex = 7;
            this.label1Threshold.Text = "Threshold: 170";
            // 
            // lbSmooth
            // 
            this.lbSmooth.AutoSize = true;
            this.lbSmooth.Location = new System.Drawing.Point(706, 17);
            this.lbSmooth.Name = "lbSmooth";
            this.lbSmooth.Size = new System.Drawing.Size(55, 13);
            this.lbSmooth.TabIndex = 11;
            this.lbSmooth.Text = "Smooth: 1";
            // 
            // trackSmooth
            // 
            this.trackSmooth.Location = new System.Drawing.Point(767, 12);
            this.trackSmooth.Maximum = 11;
            this.trackSmooth.Minimum = 1;
            this.trackSmooth.Name = "trackSmooth";
            this.trackSmooth.Size = new System.Drawing.Size(127, 45);
            this.trackSmooth.SmallChange = 2;
            this.trackSmooth.TabIndex = 12;
            this.trackSmooth.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackSmooth.Value = 1;
            this.trackSmooth.Scroll += new System.EventHandler(this.trackSmooth_Scroll);
            // 
            // trackDilate
            // 
            this.trackDilate.Location = new System.Drawing.Point(983, 12);
            this.trackDilate.Maximum = 11;
            this.trackDilate.Minimum = 1;
            this.trackDilate.Name = "trackDilate";
            this.trackDilate.Size = new System.Drawing.Size(127, 45);
            this.trackDilate.TabIndex = 14;
            this.trackDilate.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackDilate.Value = 1;
            this.trackDilate.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // lbDilate
            // 
            this.lbDilate.AutoSize = true;
            this.lbDilate.Location = new System.Drawing.Point(922, 17);
            this.lbDilate.Name = "lbDilate";
            this.lbDilate.Size = new System.Drawing.Size(46, 13);
            this.lbDilate.TabIndex = 13;
            this.lbDilate.Text = "Dilate: 1";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(1141, 21);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(95, 17);
            this.checkBox1.TabIndex = 15;
            this.checkBox1.Text = "Draw contours";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // picROI
            // 
            this.picROI.Location = new System.Drawing.Point(1327, 63);
            this.picROI.Name = "picROI";
            this.picROI.Size = new System.Drawing.Size(172, 167);
            this.picROI.TabIndex = 16;
            this.picROI.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1537, 810);
            this.Controls.Add(this.picROI);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.trackDilate);
            this.Controls.Add(this.lbDilate);
            this.Controls.Add(this.trackSmooth);
            this.Controls.Add(this.lbSmooth);
            this.Controls.Add(this.label1Threshold);
            this.Controls.Add(this.trackBarThreshold);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_Stop);
            this.Controls.Add(this.btn_Start);
            this.Controls.Add(this.btn_Brows);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackSmooth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackDilate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picROI)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_Brows;
        private System.Windows.Forms.Button btn_Start;
        private System.Windows.Forms.Button btn_Stop;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TrackBar trackBarThreshold;
        private System.Windows.Forms.Label label1Threshold;
        private System.Windows.Forms.Label lbSmooth;
        private System.Windows.Forms.TrackBar trackSmooth;
        private System.Windows.Forms.TrackBar trackDilate;
        private System.Windows.Forms.Label lbDilate;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.PictureBox picROI;
    }
}

