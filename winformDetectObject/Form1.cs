﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using Alturos.Yolo;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;

namespace winformDetectObject
{
    public partial class Form1 : Form
    {
        Capture capture;
        int threshold = 170;
        int smooth = 1;
        int dilate = 1;
        public Form1()
        {
            InitializeComponent();
        }
        void LoadVideo()
        {
            if (capture == null)
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "Video mp4|*.mp4";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    capture = new Capture(ofd.FileName);
                }
            }
            capture.ImageGrabbed += Capture_ImageGrabbed;
            capture.Start();
        }

        private void Capture_ImageGrabbed(object sender, EventArgs e)
        {
            try
            {
                int fps = 1;
                if (capture == null)
                    return;
                else
                    fps = (int)capture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps);

                Mat m = new Mat();
                capture.Retrieve(m);
                Image<Bgr,byte> imgColor =  DetectVehicel(m.ToImage<Bgr, byte>());
                Image<Bgr, byte> imgout = m.ToImage<Bgr, byte>().Clone();
                DetectOto(imgColor);//imgout
                Thread.Sleep(fps);
            }
            catch (Exception)
            {
                capture.Stop();
                capture = null;
            }
        }

        void DetectOto(Image<Bgr, byte> img)
        {
            Image<Bgr, byte> imgout = img.Clone();
            CvInvoke.Threshold(imgout, imgout, threshold, 255, ThresholdType.Binary);
            imgout = imgout.SmoothMedian(smooth);
            imgout = imgout.Dilate(dilate);

            Image<Gray, byte> imgGray = imgout.Convert<Gray, byte>();
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat m = new Mat();
            CvInvoke.FindContours(imgGray, contours, m, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
            for (int i = 0; i < contours.Size; i++)
            {
                double premimeter = CvInvoke.ArcLength(contours[i], true);
                VectorOfPoint approx = new VectorOfPoint();
                CvInvoke.ApproxPolyDP(contours[i], approx, 0.04 * premimeter, true);
                if(checkBox1.Checked == true)
                    CvInvoke.DrawContours(img, contours, i, new MCvScalar(255, 0, 255), 2);
                Rectangle brect = CvInvoke.BoundingRectangle(contours[i]);
                double ar = (double)brect.Width / (double)brect.Height;
                if (
                    premimeter > 400 && premimeter <1500
                    &&  ar >= 1.4
                    //&& brect.Width > 400
                    //&& brect.Width < 500
                    //&& brect.Height > 50
                    //&& brect.Height < 200
                    )
                {
                    CvInvoke.Rectangle(img, brect, new MCvScalar(0, 255, 255), 2);

                    //moments center of the shape
                    var moments = CvInvoke.Moments(contours[i]);
                    int x = (int)(moments.M10 / moments.M00);
                    int y = (int)(moments.M01 / moments.M00);
                    if (approx.Size >= 5)
                    {
                        CvInvoke.PutText(img, string.Format("{0} - {1}", Math.Round(ar,2), Math.Round(premimeter, 2)), new Point(x, y),
                            Emgu.CV.CvEnum.FontFace.HersheySimplex, 1, new MCvScalar(0, 255, 255));
                    }
                }
            }
            pictureBox2.Image = img.Bitmap;
        }
        void DetectXeMay(Image<Bgr, byte> img)
        {
            Image<Bgr, byte> imgout = img.Clone();
            CvInvoke.Threshold(imgout, imgout, threshold, 255, ThresholdType.BinaryInv);
            imgout = imgout.SmoothMedian(smooth);
            imgout = imgout.Dilate(dilate);

            Image<Gray, byte> imgGray = imgout.Convert<Gray, byte>();
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat m = new Mat();
            CvInvoke.FindContours(imgGray, contours, m, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
            for (int i = 0; i < contours.Size; i++)
            {
                double premimeter = CvInvoke.ArcLength(contours[i], true);
                VectorOfPoint approx = new VectorOfPoint();
                CvInvoke.ApproxPolyDP(contours[i], approx, 0.04 * premimeter, true);
                CvInvoke.DrawContours(img, contours, i, new MCvScalar(255, 0, 255), 2);
                Rectangle brect = CvInvoke.BoundingRectangle(contours[i]);
                double ar = brect.Width / brect.Height;
                if (
                    premimeter >200
                    && ar < 3
                    && brect.Width > 50
                    && brect.Width < 500
                    && brect.Height > 50
                    && brect.Height < 200
                    )
                {
                    CvInvoke.Rectangle(img, brect, new MCvScalar(0, 0, 255), 2);

                    //moments center of the shape
                    var moments = CvInvoke.Moments(contours[i]);
                    int x = (int)(moments.M10 / moments.M00);
                    int y = (int)(moments.M01 / moments.M00);
                    if (approx.Size >= 5)
                    {
                        CvInvoke.PutText(img, "Car", new Point(x, y),
                            Emgu.CV.CvEnum.FontFace.HersheySimplex, 0.5, new MCvScalar(0, 0, 255));
                    }
                }
            }
            pictureBox2.Image = img.Bitmap;
        }
        private Image<Bgr, byte> DetectVehicel(Image<Bgr, byte> img)
        {
            Image<Gray, byte> imgSmooth = img.SmoothGaussian(5).Convert<Gray, byte>().ThresholdBinaryInv(new Gray(100), new Gray(255));
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat m = new Mat();
            CvInvoke.FindContours(imgSmooth, contours, m, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
            for (int i = 0; i < contours.Size ; i++)
            {
                double premimeter = CvInvoke.ArcLength(contours[i],true);
                VectorOfPoint approx = new VectorOfPoint();
                CvInvoke.ApproxPolyDP(contours[i], approx, 0.04*premimeter,true);
                if(checkBox1.Checked == true)
                    CvInvoke.DrawContours(img, contours, i, new MCvScalar(255, 0, 255),2);
                Rectangle brect = CvInvoke.BoundingRectangle(contours[i]);
                double ar = (double) brect.Width / (double)brect.Height;
                if (
                    premimeter >200 && premimeter < 400
                    && ar>1 && ar <3
                    //&& brect.Width > 50
                    //&& brect.Width < 200
                    //&& brect.Height > 50
                    //&& brect.Height < 200
                    ) {
                    CvInvoke.Rectangle(img, brect, new MCvScalar(0, 0, 255), 2);
                    //moments center of the shape
                    var moments = CvInvoke.Moments(contours[i]);
                    int x = (int)(moments.M10 / moments.M00);
                    int y = (int)(moments.M01 / moments.M00);
                   // if (approx.Size >= 5)
                    {
                        CvInvoke.PutText(img, "Motor", new Point(x, y),
                            Emgu.CV.CvEnum.FontFace.HersheySimplex, 0.5, new MCvScalar(0, 0, 255));
                    }
                }
            }
            pictureBox1.Image = img.Bitmap;
            return img;
        }
        #region event FUNC 
        private void btn_Start_Click(object sender, EventArgs e)
        {
            if(capture!=null)
             capture.Start();
        }

        private void btn_Pause_Click(object sender, EventArgs e)
        {
            if (capture != null)
            {
                capture.Pause();
            }
        }

        private void btn_Stop_Click(object sender, EventArgs e)
        {
            if (capture != null)
            {
                capture.Stop();
                capture = null;
            }
        }

        private void btn_Brows_Click(object sender, EventArgs e)
        {
            LoadVideo();
        }
        #endregion
        #region dectect car and motos
        Image<Bgr, byte> ThresholdIMG(Image<Bgr, byte> imgOrg)
        {
            Image<Bgr, byte> imgout = imgOrg.Clone();
            CvInvoke.Threshold(imgout, imgout, threshold, 255, Emgu.CV.CvEnum.ThresholdType.Binary);

            Mat element = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle,
                new System.Drawing.Size(3, 3), new System.Drawing.Point(-1, -1));
            CvInvoke.Dilate(imgout, imgout
             , element
             , new System.Drawing.Point(-1, -1)
             , dilate
             , BorderType.Default
             , new MCvScalar(255, 255, 255));

            //CvInvoke.Dilate(imgOrg, imgOrg
            // , element
            // , new System.Drawing.Point(-1, -1)
            // , 1
            // , BorderType.Default
            //  , new MCvScalar(255, 255, 255));

            return imgout;
        }
        void DetectTag2(Image<Bgr, byte> img)
        {
            //threshod
            Image<Bgr, byte> tmp = ThresholdIMG(img);
            Image<Bgr, byte> test = tmp.Copy();
            //Smooth
            Image<Gray, byte> imgGray = test.Convert<Gray, byte>().SmoothGaussian(smooth);
            //.ThresholdBinaryInv(new Gray(100), new Gray(255));

            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat m = new Mat();
            //Image<Gray, byte> imgOut = new Image<Gray, byte>(img.Width, img.Height, new Gray(0));
            CvInvoke.FindContours(imgGray, contours, m, RetrType.External, ChainApproxMethod.ChainApproxSimple);
            //CvInvoke.DrawContours(img, contours, -1, new MCvScalar(255, 0, 255));
            for (int i = 0; i < contours.Size; i++)
            {
                double perimeter = CvInvoke.ArcLength(contours[i], true);
                VectorOfPoint approx = new VectorOfPoint();
                CvInvoke.ApproxPolyDP(contours[i], approx, 0.04 * perimeter, true);
                CvInvoke.DrawContours(img, contours, i, new MCvScalar(255, 0, 255), 2);
                Rectangle brect = CvInvoke.BoundingRectangle(contours[i]);
                double ar = brect.Width / brect.Height;
                if (
                    perimeter > 200
                    && ar < 3
                    && brect.Width > 50
                    //&& brect.Width < 500
                    && brect.Height > 50
                    //&& brect.Height < 200
                    )
                {
                    //moments center of the shape
                    var moments = CvInvoke.Moments(contours[i]);
                    int x = (int)(moments.M10 / moments.M00);
                    int y = (int)(moments.M01 / moments.M00);
                    if (approx.Size >= 5)
                    {
                        CvInvoke.Rectangle(img, brect, new MCvScalar(0, 255, 0), 2);
                        Image<Bgr, byte> imgROI = img.Clone();
                        imgROI.ROI = new Rectangle(brect.X, brect.Y, brect.Width, brect.Height);

                        picROI.Image = imgROI.Bitmap;

                        CvInvoke.PutText(img, ((int)perimeter).ToString(), new Point(x, y),
                            Emgu.CV.CvEnum.FontFace.HersheySimplex,1, new MCvScalar(0, 255, 255),2);

                        CvInvoke.Rectangle(imgGray, brect, new MCvScalar(0, 255, 0), 2);

                        CvInvoke.PutText(imgGray, ((int)perimeter).ToString(), new Point(x, y),
                            Emgu.CV.CvEnum.FontFace.HersheySimplex, 1, new MCvScalar(0, 255, 255), 2);
                    }
                }
            }
            if (checkBox1.Checked)
                pictureBox2.Image = img.Bitmap;
            else
                pictureBox2.Image = imgGray.Bitmap;
        }

        void Tag2(Image<Bgr, byte> img)
        {
            //Sobel(0, 1, 3) là ngang | (1,0,3) là dọc
            Image<Gray, byte> gray = img.Convert<Gray, byte>().ThresholdBinary(new Gray(threshold), new Gray(255));
            
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat m = new Mat();
            Image<Gray, byte> imgOut = new Image<Gray, byte>(img.Width, img.Height, new Gray(0));
            CvInvoke.FindContours(gray, contours, m, RetrType.External, ChainApproxMethod.ChainApproxSimple);
            CvInvoke.DrawContours(imgOut, contours, -1, new MCvScalar(255, 0, 255));
            pictureBox2.Image = imgOut.Bitmap;
        }
        #endregion

        private void trackBarThreshold_Scroll(object sender, EventArgs e)
        {
            threshold = trackBarThreshold.Value;
            label1Threshold.Text = "Threshold: "  + trackBarThreshold.Value.ToString();
        }

        private void trackSmooth_Scroll(object sender, EventArgs e)
        {
            smooth = trackSmooth.Value;
            lbSmooth.Text = "Smooth: " + trackSmooth.Value.ToString();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            dilate = trackDilate.Value;
            lbDilate.Text = "Dilate: " + trackDilate.Value.ToString();
        }
    }

}
